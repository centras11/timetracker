<?php

namespace App\Filter;

use App\Entity\User;

class TimeTrackFilter
{
    private ?User $user = null;
    private bool $orderByDate = true;

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): void
    {
        $this->user = $user;
    }

    public function isOrderByDate(): bool
    {
        return $this->orderByDate;
    }

    public function setOrderByDate(bool $orderByDate): void
    {
        $this->orderByDate = $orderByDate;
    }
}
