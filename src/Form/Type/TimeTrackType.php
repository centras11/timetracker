<?php

namespace App\Form\Type;

use App\Entity\TimeTrack;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TimeTrackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, ['required' => true])
            ->add('comment', TextType::class)
            ->add('date', DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'required' => true,
            ])
            ->add('timeSpent', IntegerType::class, ['required' => true]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $defaults = [
            'data_class' => TimeTrack::class,
            'csrf_protection' => false,
        ];
        $resolver->setDefaults($defaults);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'time_track_edit';
    }
}
