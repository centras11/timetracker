<?php

namespace App\Exception;

use Symfony\Component\Form\FormErrorIterator;

class FormValidationException extends \RuntimeException
{
    public array $errors;
    protected $message = 'Form validation failed';

    public function __construct(FormErrorIterator $errors)
    {
        parent::__construct();

        $this->setErrors($errors);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param FormErrorIterator $errors
     */
    public function setErrors(FormErrorIterator $errors): void
    {
        $errorMessages = [];
        foreach ($errors as $error) {
            $errorMessages[] = $error->getMessage();
        }

        $this->errors = $errorMessages;
    }
}
