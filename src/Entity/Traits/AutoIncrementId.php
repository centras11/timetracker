<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait AutoIncrementId
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    private ?int $id;

    public function getId(): int
    {
        return $this->id;
    }
}
