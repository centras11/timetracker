<?php

namespace App\Entity;

use App\Entity\Traits\AutoIncrementId;
use App\Repository\TimeTrackRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 *
 * @ORM\Table(
 *     name="time_track",
 *     indexes={
 *         @ORM\Index(name="user_date_idx", columns={"user_id", "date"}),
 * })
 *
 * @ORM\Entity(repositoryClass=TimeTrackRepository::class)
 */
class TimeTrack implements BaseEntityInterface
{
    use AutoIncrementId;

    public const GROUP_SHOW = 'time_track_show';
    public const GROUP_LIST = 'time_track_list';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({self::GROUP_SHOW, self::GROUP_LIST})
     */
    private ?string $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({self::GROUP_SHOW, self::GROUP_LIST})
     */
    private ?string $comment;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({self::GROUP_SHOW, self::GROUP_LIST})
     */
    private ?DateTimeInterface $date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({self::GROUP_SHOW, self::GROUP_LIST})
     */
    private ?int $timeSpent;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Groups({self::GROUP_SHOW})
     */
    private ?User $user;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTimeSpent(): ?int
    {
        return $this->timeSpent;
    }

    public function setTimeSpent(?int $timeSpent): self
    {
        $this->timeSpent = $timeSpent;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
