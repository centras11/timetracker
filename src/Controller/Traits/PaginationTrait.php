<?php

namespace App\Controller\Traits;

use Symfony\Component\HttpFoundation\Request;

trait PaginationTrait
{
    protected function getPerPageNumber(array $options, Request $request): int
    {
        $current = $request->query->get('per_page');

        return ($current && in_array($current, $options, false)) ? $current : $options[0];
    }
}