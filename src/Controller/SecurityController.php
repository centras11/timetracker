<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\ResponseService;
use App\Service\UserService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class SecurityController extends AbstractController
{
    private ResponseService $responseService;
    private UserService $userService;

    public function __construct(ResponseService $responseService, UserService $userService)
    {
        $this->responseService = $responseService;
        $this->userService = $userService;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    #[Route('/user/create', name: 'user.create', methods: ["POST"])]
    public function create(Request $request): JsonResponse
    {
        $user = $this->userService->createUser($request);

        return $this->responseService->createResponse($user, Response::HTTP_OK, ['groups' => User::GROUP_SHOW]);
    }

    #[Route('/login', name: 'security.login', methods: ["POST"])]
    public function login(): JsonResponse
    {
        return $this->responseService->createResponse(
            $this->getUser(),
            Response::HTTP_OK,
            ['groups' => User::GROUP_SHOW]
        );
    }

    #[Route('/logout', name: 'security.logout', methods: ["GET"])]
    public function logout(): void
    {
    }
}
