<?php

namespace App\Controller;

use App\Controller\Traits\PaginationTrait;
use App\Entity\TimeTrack;
use App\Exception\FormValidationException;
use App\Service\ResponseService;
use App\Service\TimeTrackService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\LengthRequiredHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class TimeTrackController extends AbstractController
{
    use PaginationTrait;

    private ResponseService $responseService;
    private TimeTrackService $timeTrackService;
    private array $paginationOptions;

    public function __construct(
        ResponseService $responseService,
        TimeTrackService $timeTrackService,
        array $paginationOptions
    ) {
        $this->responseService = $responseService;
        $this->timeTrackService = $timeTrackService;
        $this->paginationOptions = $paginationOptions;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     * @throws LengthRequiredHttpException|FormValidationException
     */
    #[Route('/time/track/create', name: 'time_track.create', methods: ["POST"])]
    public function create(Request $request): JsonResponse
    {
        $timeTrack = $this->timeTrackService->create($request, $this->getUser());

        return $this->responseService->createResponse(
            $timeTrack,
            Response::HTTP_OK,
            ['groups' => TimeTrack::GROUP_SHOW]
        );
    }

    #[Route('/time/track/list', name: 'time_track.list', methods: ["GET"])]
    public function list(Request $request): JsonResponse
    {
        $perPage = $this->getPerPageNumber($this->paginationOptions, $request);
        $data = $this->timeTrackService->getListForUser(
            $this->getUser(),
            $request->query->get('page', 1),
            $perPage
        );

        return $this->responseService->createResponse($data, Response::HTTP_OK, ['groups' => TimeTrack::GROUP_LIST]);
    }
}
