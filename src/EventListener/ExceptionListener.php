<?php

namespace App\EventListener;

use App\Service\ResponseService;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener
{
    private ResponseService $responseService;

    public function __construct(ResponseService $responseService)
    {
        $this->responseService = $responseService;
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        $response = $this->responseService->createErrorResponse($exception->getMessage());
        $event->setResponse($response);
    }
}
