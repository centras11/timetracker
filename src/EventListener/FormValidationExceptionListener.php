<?php

namespace App\EventListener;

use App\Exception\FormValidationException;
use App\Service\ResponseService;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class FormValidationExceptionListener
{
    private ResponseService $responseService;

    public function __construct(ResponseService $responseService)
    {
        $this->responseService = $responseService;
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if (!$event->getThrowable() instanceof FormValidationException) {
            return;
        }

        $response = $this->responseService->createErrorResponse($exception->getMessage(), $exception->getErrors());
        $event->setResponse($response);
    }
}
