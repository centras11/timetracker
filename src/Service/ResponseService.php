<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class ResponseService
{
    public const STATUS_SUCCESS = 'success';
    public const STATUS_FAILED = 'failed';

    protected SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function createErrorResponse(
        string $error,
        $details = null,
        int $code = Response::HTTP_INTERNAL_SERVER_ERROR,
        array $context = [],
        array $headers = []
    ): JsonResponse {
        $response = $this->formatErrorData($error, $details);
        $serializedData = $this->serializer->serialize($response, 'json', $context);

        return new JsonResponse($serializedData, $code, $headers, true);
    }

    public function createResponse(
        $data,
        int $code = Response::HTTP_OK,
        array $context = [],
        array $headers = []
    ): JsonResponse {
        $response = $this->formatData($data);
        $serializedData = $this->serializer->serialize($response, 'json', $context);

        return new JsonResponse($serializedData, $code, $headers, true);
    }

    protected function formatErrorData(string $error, $details = null): array
    {
        $data = [];
        $data['error'] = $error;

        if ($details) {
            $data['details'] = $details;
        }

        return $this->formatData($data, self::STATUS_FAILED);
    }

    protected function formatData($data, string $status = self::STATUS_SUCCESS): array
    {
        return [
            'data' => $data,
            'status' => $status,
        ];
    }
}
