<?php

namespace App\Service;

use App\Entity\User;
use App\Form\Type\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserService
{
    protected UserRepository $userRepository;
    protected FormService $formService;
    protected UserPasswordHasherInterface $passwordHasher;

    public function __construct(
        UserRepository $userRepository,
        FormService $formService,
        UserPasswordHasherInterface $passwordHasher
    ) {
        $this->userRepository = $userRepository;
        $this->formService = $formService;
        $this->passwordHasher = $passwordHasher;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function createUser(Request $request): User
    {
        /** @var User $user */
        $user = $this->formService->processForm($request, UserType::class);
        $hashedPassword = $this->passwordHasher->hashPassword(
            $user,
            $user->getPlainPassword()
        );
        $user->setPassword($hashedPassword);
        $user->eraseCredentials();
        $this->userRepository->save($user);

        return $user;
    }
}
