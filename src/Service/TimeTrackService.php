<?php

namespace App\Service;

use App\Entity\TimeTrack;
use App\Entity\User;
use App\Exception\FormValidationException;
use App\Filter\TimeTrackFilter;
use App\Form\Type\TimeTrackType;
use App\Repository\TimeTrackRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\LengthRequiredHttpException;

class TimeTrackService
{
    protected TimeTrackRepository $timeTrackRepository;
    protected TimeTrackFilter $timeTrackFilter;
    protected PaginatorInterface $paginator;
    protected FormService $formService;

    public function __construct(
        TimeTrackRepository $timeTrackRepository,
        TimeTrackFilter $timeTrackFilter,
        PaginatorInterface $paginator,
        FormService $formService
    ) {
        $this->timeTrackRepository = $timeTrackRepository;
        $this->timeTrackFilter = $timeTrackFilter;
        $this->paginator = $paginator;
        $this->formService = $formService;
    }

    public function getListForUser(User $user, int $page, int $limit): PaginationInterface
    {
        $this->timeTrackFilter->setUser($user);

        return $this->getPaginatedList($page, $limit);
    }

    private function getPaginatedList(int $page, int $limit): PaginationInterface
    {
        $query = $this->timeTrackRepository->getQb($this->timeTrackFilter);

        return $this->paginator->paginate($query, $page, $limit, ['distinct' => false]);
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     * @throws LengthRequiredHttpException|FormValidationException
     */
    public function create(Request $request, User $user): TimeTrack
    {
        /** @var TimeTrack $timeTrack */
        $timeTrack = $this->formService->processForm($request, TimeTrackType::class);
        $timeTrack->setUser($user);
        $this->timeTrackRepository->save($timeTrack);

        return $timeTrack;
    }
}
