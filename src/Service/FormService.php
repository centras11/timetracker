<?php

namespace App\Service;

use App\Entity\BaseEntityInterface;
use App\Exception\FormValidationException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\LengthRequiredHttpException;

class FormService
{
    protected FormFactoryInterface $formFactory;

    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @throws LengthRequiredHttpException|FormValidationException
     */
    public function processForm(Request $request, $type): BaseEntityInterface
    {
        $form = $this->formFactory->create($type);
        $body = $this->getBody($request);
        $data = json_decode($body, true);
        $form->submit($data);

        if (!$form->isValid()) {
            throw new FormValidationException($form->getErrors(true));
        }

        return $form->getData();
    }

    /**
     * @throws LengthRequiredHttpException
     */
    private function getBody(Request $request): string
    {
        $body = $request->getContent();

        if (empty($body)) {
            throw new LengthRequiredHttpException();
        }

        return $body;
    }
}
