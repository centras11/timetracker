<?php

namespace App\Repository;

use App\Entity\TimeTrack;
use App\Filter\TimeTrackFilter;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TimeTrack|null find($id, $lockMode = null, $lockVersion = null)
 * @method TimeTrack|null findOneBy(array $criteria, array $orderBy = null)
 * @method TimeTrack[]    findAll()
 * @method TimeTrack[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TimeTrackRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TimeTrack::class);
    }

    public function getQB(?TimeTrackFilter $filter): QueryBuilder
    {
        $builder = $this->getEntityManager()->createQueryBuilder();
        $builder
            ->select('tt')
            ->from(TimeTrack::class, 'tt');

        if (null !== $filter) {
            $this->applyFilter($filter, $builder);
        }

        return $builder;
    }

    public function applyFilter(TimeTrackFilter $filter, $builder): QueryBuilder
    {
        if (null !== $filter->getUser()) {
            $builder
                ->innerJoin('tt.user', 'u')
                ->andWhere('u.id = :user')
                ->setParameter('user', $filter->getUser());
        }

        if ($filter->isOrderByDate()) {
            $builder
                ->addOrderBy('tt.date', 'DESC');
        }

        return $builder;
    }
}
