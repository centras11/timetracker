<?php

namespace App\Repository;

use App\Entity\BaseEntityInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

abstract class BaseRepository extends ServiceEntityRepository
{
    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function save(BaseEntityInterface $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);

        if ($flush) {
            $this->_em->flush();
        }
    }
}
