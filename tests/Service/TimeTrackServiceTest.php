<?php

namespace App\Tests\Service;

use App\Entity\TimeTrack;
use App\Entity\User;
use App\Exception\FormValidationException;
use App\Filter\TimeTrackFilter;
use App\Form\Type\TimeTrackType;
use App\Repository\TimeTrackRepository;
use App\Service\FormService;
use App\Service\TimeTrackService;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\PaginatorInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\Request;

class TimeTrackServiceTest extends TestCase
{
    protected TimeTrackRepository $timeTrackRepository;
    protected TimeTrackFilter $timeTrackFilter;
    protected PaginatorInterface $paginator;
    protected FormService $formService;
    protected TimeTrackService $timeTrackService;

    public function setUp(): void
    {
        $this->timeTrackRepository = $this->createMock(TimeTrackRepository::class);
        $this->timeTrackFilter = $this->createMock(TimeTrackFilter::class);
        $this->paginator = $this->createMock(PaginatorInterface::class);
        $this->formService = $this->createMock(FormService::class);
        $this->timeTrackService = new TimeTrackService(
            $this->timeTrackRepository,
            $this->timeTrackFilter,
            $this->paginator,
            $this->formService
        );
    }

    public function testItShouldGetListForUser(): void
    {
        $user = new User();
        $page = 1;
        $limit = 10;
        $queryBuilder = $this->createMock(QueryBuilder::class);

        $this->timeTrackFilter->expects(self::once())
            ->method('setUser')
            ->with($user);

        $this->timeTrackRepository->expects(self::once())
            ->method('getQb')
            ->willReturn($queryBuilder);

        $this->paginator->expects(self::once())
            ->method('paginate')
            ->with($queryBuilder, $page, $limit, ['distinct' => false]);

        $this->timeTrackService->getListForUser($user, $page, $limit);
    }

    public function testItShouldCreate(): void
    {
        $user = new User();
        $timeTrack = new TimeTrack();
        $request = $this->createMock(Request::class);

        $this->formService->expects(self::once())
            ->method('processForm')
            ->with($request, TimeTrackType::class)
            ->willReturn($timeTrack);

        $this->timeTrackRepository->expects(self::once())
            ->method('save')
            ->with($timeTrack);

        $this->timeTrackService->create($request, $user);
    }

    public function testItShouldThrowExceptionOnCreateFail(): void
    {
        $user = new User();
        $request = $this->createMock(Request::class);
        $errors = $this->createMock(FormErrorIterator::class);

        $this->formService->expects(self::once())
            ->method('processForm')
            ->with($request, TimeTrackType::class)
            ->willThrowException(new FormValidationException($errors));

        $this->expectException(FormValidationException::class);
        $this->expectExceptionMessage('Form validation failed');

        $this->timeTrackService->create($request, $user);
    }
}
