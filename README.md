# Time Tracking App - job interview task for baltpool.eu

### Used Stack:
* PHP 8.0;
* MySql 5.7;
* Symfony 5.3;

### About solution:
* API based APP to Track Time;
* Has two Entities: User for user data and TimeTrack for time tracking;
* ResponseService was introduced in order always to have same structure for all responses;
* FormService was introduced to share form handling functionality between Entities;
* BaseRepository was introduced to share some common logic of Repositories (save Entity);  
* All errors and exceptions are handled by Exception Listeners;

### Some Todos left:
* report feature;
* frontend; 
* some (a lot :) ) code miss tests;